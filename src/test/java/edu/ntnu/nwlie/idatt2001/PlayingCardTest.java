package edu.ntnu.nwlie.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlayingCardTest {

    @Test
    @DisplayName("Test of method getSuit()")
    public void getSuit() {
        PlayingCard card = new PlayingCard('C', 10);
        assertTrue(Character.toString(card.getSuit()).equals("C"));
    }

    @Test
    @DisplayName("Test of method getFace()")
    public void getFace() {
        PlayingCard card = new PlayingCard('C', 10);
        assertTrue(card.getFace() == 10);
    }
}

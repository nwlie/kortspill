package edu.ntnu.nwlie.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HandOfCardsTest {

    @Test
    @DisplayName("Check the sum of the hand")
    public void checkSum() {
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards(deck.dealHand(52));
        assertTrue(hand.sum() == 364);
    }

    @Test
    @DisplayName("Check for hearts")
    public void checkForHearts() {
        HandOfCards hand = new HandOfCards(0);
        assertTrue(hand.hearts().equals("H10"));
    }

    @Test
    @DisplayName("Check for queen of spades")
    public void checkForQueenOfSpades() {
        HandOfCards hand = new HandOfCards(1);
        assertTrue(hand.queenOfSpades());
    }

    @Nested
    class flush {
        @Test
        @DisplayName("Check for flush")
        public void checkForFlush() {
            HandOfCards hand = new HandOfCards(2);
            assertTrue(hand.flush());
        }

        @Test
        @DisplayName("Check for not flush")
        public void checkForNotFlush() {
            HandOfCards hand = new HandOfCards(1);
            assertFalse(hand.flush());
        }
    }


}

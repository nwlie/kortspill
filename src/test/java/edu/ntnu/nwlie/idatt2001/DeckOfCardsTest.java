package edu.ntnu.nwlie.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class DeckOfCardsTest {

    @Test
    @DisplayName("Check that a deck of 52 cards is created")
    public void checkDeckCount() {
        DeckOfCards deck = new DeckOfCards();
        assertTrue(deck.getNumberOfCards() == 52);
    }

    @Nested
    class dealingAHandFromTheDeck {

        @Test
        @DisplayName("Check that a hand can be dealt")
        public void dealHand() {
            DeckOfCards deck = new DeckOfCards();
            assertTrue(deck.dealHand(5).size() == 5);
        }

        @Test
        @DisplayName("Check that cards are removed from the deck when dealt")
        public void removeDealtCards() {
            DeckOfCards deck = new DeckOfCards();
            ArrayList<PlayingCard> hand = deck.dealHand(5);
            for (PlayingCard card : hand) {
                assertFalse(deck.getDeck().contains(card));
            }
        }
    }
}

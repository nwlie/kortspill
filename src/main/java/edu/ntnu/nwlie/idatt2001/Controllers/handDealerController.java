package edu.ntnu.nwlie.idatt2001.Controllers;

import edu.ntnu.nwlie.idatt2001.DeckOfCards;
import edu.ntnu.nwlie.idatt2001.HandOfCards;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class handDealerController {

    @FXML
    private TextField sum;
    @FXML
    private TextField flush;
    @FXML
    private TextField hearts;
    @FXML
    private TextField queenOfSpades;
    @FXML
    private Label card1;
    @FXML
    private Label card2;
    @FXML
    private Label card3;
    @FXML
    private Label card4;
    @FXML
    private Label card5;
    @FXML
    private Label card6;
    @FXML
    private Label card7;
    @FXML
    private Label remainingCards;

    private DeckOfCards deck;
    private HandOfCards hand;
    private String[] listOfCards;

    /**
     * Creates a new deck when the program starts
     */
    @FXML
    public void initialize() {
        deck = new DeckOfCards();
        remainingCards.setText(String.valueOf(deck.getNumberOfCards()));
    }

    /**
     * Deals a new hand, and updates all relevant fields. If the remaining cards in the
     * deck is not enough to deal a hand, a new deck is created.
     */
    @FXML
    public void dealNewHand() {
        if(Integer.parseInt(remainingCards.getText()) < 7) {
            deck = new DeckOfCards();
        }

        hand = new HandOfCards(deck.dealHand(7));
        remainingCards.setText(String.valueOf(deck.getNumberOfCards()));
        listOfCards = hand.handAsString().split("\\s+");

        card1.setText(listOfCards[0]);
        card2.setText(listOfCards[1]);
        card3.setText(listOfCards[2]);
        card4.setText(listOfCards[3]);
        card5.setText(listOfCards[4]);
        card6.setText(listOfCards[5]);
        card7.setText(listOfCards[6]);
    }

    /**
     * Checks the hand sum, hearts, Queen of Spades and flush. Updates the relevant fields
     */
    @FXML
    public void checkNewHand() {
        sum.setText(String.valueOf(hand.sum()));
        hearts.setText(hand.hearts());

        if(hand.flush()) {
            flush.setText("Yes");
        } else {
            flush.setText("No");
        }

        if(hand.queenOfSpades()) {
            queenOfSpades.setText("Yes");
        } else {
            queenOfSpades.setText("No");
        }
    }
}

package edu.ntnu.nwlie.idatt2001;

import java.util.ArrayList;

/**
 * Represents a hand of cards. A hand of cards is dealt from a deck, and each card is represented
 * using the PlayingCard-class.
 */
public class HandOfCards {

    private ArrayList<PlayingCard> hand ;

    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * A constructor used to achieve consistency in tests
     * @param value integer used to represent which test should run
     */
    public HandOfCards(int value) {
        this.hand = new ArrayList<PlayingCard>();
        if(value == 0) {
            this.hand.add(new PlayingCard('H', 10));
        } else if(value == 1){
            this.hand.add(new PlayingCard('S', 12));
        } else if(value == 2) {
            for(int i = 2; i < 8; i++) {
                this.hand.add(new PlayingCard('S', i));
            }
        }
    }

    /**
     * Gets the sum of all the cards in the hand
     * @return The sum of all the cards in the hand
     */
    public int sum() {
        return hand.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Returns a String with all the hearts in the hand
     * @return A String with all the hearts in the hand. Returns "No hearts" if no hearts
     */
    public String hearts() {
        StringBuilder sb = new StringBuilder();
        for (PlayingCard card : hand) {
            if(Character.toString(card.getSuit()).equals("H")) {
                sb.append(card.getAsString());
            }
        }
        if(sb.isEmpty()) {
            return ("No hearts");
        }
        return sb.toString();
    }

    /**
     * Checks if the hand contains the card Queen of Spades
     * @return True if contains, false if it doesnt
     */
    public boolean queenOfSpades() {
        return hand.stream().anyMatch(card -> card.getAsString().trim().equals("S12"));
    }

    /**
     * Checks if the hand has a flush (5 cards of the same suit)
     * @return True if it does, false if it doesnt
     */
    public boolean flush() {
        int spades = 0;
        int hearts = 0;
        int diamonds = 0;
        int clubs = 0;
        String type;

        for (PlayingCard card : hand) {
            type = Character.toString(card.getSuit());
            if(type.equals("H")) {
                hearts++;
            } else if(type.equals("D")){
                diamonds++;
            } else if(type.equals("C")) {
                clubs++;
            } else{
                spades++;
            }
        }
        if(spades > 4 || hearts > 4 || diamonds > 4 || clubs > 4) {
            return true;
        }
        return false;
    }

    /**
     * Returns the hand as a string
     * @return Hand as a string
     */
    public String handAsString() {
        StringBuilder sb = new StringBuilder();
        for (PlayingCard card : hand) {
            sb.append(card.getAsString());
        }
        return sb.toString();
    }
}

package edu.ntnu.nwlie.idatt2001;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of cards. A deck has 52 cards, with values from 1-13
 * and each value in 4 different suits. The deck represents a card using from the
 * PlayingCard-class
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deck = new ArrayList<PlayingCard>();
    private Random random = new Random();

    public DeckOfCards() {
        for (char suit : suit) {
            for(int i = 1; i < 14; i++) {
                this.deck.add(new PlayingCard(suit, i));
            }
        }
    }

    /**
     * Deals a new hand, randomly selected from the remaining cards in the deck.
     * Removes the dealt cards from the deck when finished.
     * @param n The amount of cards to be dealt
     * @return An ArrayList<PlayingCard> containing the selected cards
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        ArrayList<PlayingCard> hand = new ArrayList<PlayingCard>();
        int randomInt;
        for(int i = 0; i < n; i++) {
            randomInt = random.nextInt(this.deck.size());
            hand.add(this.deck.get(randomInt));
            this.deck.remove(randomInt);
        }
        return hand;
    }

    /**
     * Returns the number of cards remaining in the deck
     * @return The number of cards remaining in the deck
     */
    public int getNumberOfCards() {
        return deck.size();
    }

    /**
     * Returns a list of the remaining deck
     * @return List of the remaining deck
     */
    public ArrayList<PlayingCard> getDeck() {
        return this.deck;
    }
}


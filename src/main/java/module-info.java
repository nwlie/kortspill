module edu.ntnu.nwlie.idatt2001.Controllers {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;

    opens edu.ntnu.nwlie.idatt2001 to javafx.fxml;
    exports edu.ntnu.nwlie.idatt2001;
    exports edu.ntnu.nwlie.idatt2001.Controllers;
    opens edu.ntnu.nwlie.idatt2001.Controllers to javafx.fxml;
}